package com.example.idadedecachorro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText caixaDeTexto;
    private Button BotaoDescobrir;
    private TextView Resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         caixaDeTexto = findViewById(R.id.digitarIdade);
         BotaoDescobrir = findViewById(R.id.buttonIdade);
         Resultado = findViewById(R.id.Resultado);


         BotaoDescobrir.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                   String textodigitado = caixaDeTexto.getText().toString();

                   if(textodigitado.isEmpty()){
                       //mensagem de erro
                       Resultado.setText("Nenhum numero digitado!");
                   }
                   else{
                       int ValorDigitado = Integer.parseInt(textodigitado);


                       int res = ValorDigitado * 7;
                       Resultado.setText("Seu cachorro tem " + res + " anos de idade em anos cachorreis");


                   }
             }
         });
    }
}
